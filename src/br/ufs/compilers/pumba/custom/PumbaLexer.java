package br.ufs.compilers.pumba.custom;

import java.io.IOException;
import java.io.PushbackReader;

import br.ufs.compilers.pumba.lexer.Lexer;
import br.ufs.compilers.pumba.lexer.LexerException;
import br.ufs.compilers.pumba.node.EOF;
import br.ufs.compilers.pumba.node.InvalidToken;
import br.ufs.compilers.pumba.node.TComment;
import br.ufs.compilers.pumba.node.TCommentBegin;
import br.ufs.compilers.pumba.node.TCommentEnd;
import br.ufs.compilers.pumba.node.TCommentblockerror;
import br.ufs.compilers.pumba.node.TStringEnd;
import br.ufs.compilers.pumba.node.TStringInit;
import br.ufs.compilers.pumba.node.TString;

public class PumbaLexer extends Lexer {
	private int unmatchedBlockComment;

	private StringBuilder stringText;
	private TString string;

	public PumbaLexer(PushbackReader in) {
		super(in);
	}

	public State getState() {
		return this.state;
	}

	@Override
	protected void filter() throws LexerException, IOException {
		super.filter();

		if (state.equals(State.COMMENT)) {
			if (token instanceof EOF && unmatchedBlockComment != 0) {
				throw new LexerException(new InvalidToken("BlockCommentError",
						token.getLine(), token.getPos()), String.format(
						"[%d,%d] Error block comment", token.getLine(),
						token.getPos()));
			} else {
				if (token instanceof TCommentBegin) {
					unmatchedBlockComment++;
				} else if (token instanceof TCommentEnd) {
					unmatchedBlockComment--;
				}

				if (unmatchedBlockComment == 0) {
					token = new TComment("");
					state = State.NORMAL;
				} else {
					token = null;
				}
			}
		} else if (state.equals(State.NORMAL)) {
			if (token instanceof TCommentblockerror)
				throw new LexerException(new InvalidToken("BlockCommentError",
						token.getLine(), token.getPos()), String.format(
						"[%d,%d] Error block comment", token.getLine(),
						token.getPos()));
		} else if (state.equals(State.STRING)) {
			if (token instanceof EOF && stringText != null) {
				throw new LexerException(new InvalidToken("StringError",
						token.getLine(), token.getPos()),
						String.format("[%d,%d] Error String", token.getLine(),
								token.getPos()));
			} else if (token instanceof TStringInit) {
				stringText = new StringBuilder("");
				string = new TString("", token.getLine(), token.getPos());
				token = null;
			} else if (token instanceof TString) {
				stringText.append(token.getText());
				token = null;
			} else if (token instanceof TStringEnd) {
				string.setText(stringText.toString());
				token = string;
				state = State.NORMAL;
				stringText = null;
				string = null;
			}

		}

	}
}
