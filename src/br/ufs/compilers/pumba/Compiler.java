package br.ufs.compilers.pumba;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PushbackReader;

import br.ufs.compilers.pumba.custom.PumbaLexer;
import br.ufs.compilers.pumba.custom.Translation;
import br.ufs.compilers.pumba.lexer.LexerException;
import br.ufs.compilers.pumba.parser.Parser;
import br.ufs.compilers.pumba.parser.ParserException;

public class Compiler {

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Uso: java -jar pumba.jar <ARQUIVO ENTRADA>");
			System.exit(1);
		}

		String inputFile = args[0];

		PumbaLexer lex;
		Parser parser;

		try (PushbackReader input = new PushbackReader(new BufferedReader(
				new InputStreamReader(new FileInputStream(inputFile))))) {

			lex = new PumbaLexer(input);
			parser = new Parser(lex);

			parser.parse().apply(new Translation());

			System.out.println("Arquivo de entrada sintaticamente correto.");
		} catch (LexerException e) {
			System.err.println("Erro léxico: " + e.getMessage());
		} catch (ParserException e) {
			System.err.println("Erro sintático: " + e.getMessage());
		} catch (FileNotFoundException e) {
			System.err.println("Arquivo não existe: " + inputFile);
		} catch (IOException e) {
			System.err.println("Erro ao abrir ou ler o arquivo");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}